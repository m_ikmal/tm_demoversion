export const faq = {
  id: "faq",
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: "Frequently Asked Questions?",
  headline: "Some headlines here..",
  description: "Replace with collapsible card here..",
  buttonLabel: "More Questions?",
  imgStart: false,
  img: require("../../images/svg-1.svg"),
  alt: "faq",
  dark: true,
  primary: true,
  darkText: false,
};

export const claims = {
    id: "claims",
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: "Claiming For Someone?",
    headline: "Claims made easier for you",
    description: "Claiming for someone or yourself?",
    buttonLabel: "Start Here",
    imgStart: true,
    img: require("../../images/svg-2.svg"),
    alt: "claims",
    dark: false,
    primary: false,
    darkText: true,
  };