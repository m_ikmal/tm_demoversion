import styled from "styled-components";
import { Link as LinkR } from "react-router-dom";

export const ProductsContainer = styled.div`
  height: 800px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #fff;

  @media screen and (max-width: 768px) {
    height: 1100px;
  }

  @media screen and (max-width: 480px) {
    height: 1300px;
  }
`;

export const ProductsWrapper = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  align-items: center;
  grid-gap: 16px;
  padding: 0 20px;

  @media screen and (max-width: 1000px) {
    grid-template-columns: 1fr 1fr;
  }

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
    padding: 0 50px 50px;
  }
`;

export const ProductsCard = styled.div`
  background: #010606;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border-radius: 10px;
  max-height: 300px;
  padding: 30px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
  transition: all 0.2s ease-in-out;

  &:hover {
    transform: scale(1.02);
    transition: all 0.2s ease-in-out;
    cursor: pointer;
  }
`;

export const ProductsIcon = styled.img`
  height: 160px;
  width: 160px;
  margin-bottom: 10px;
`;

export const ProductsH1 = styled.h1`
  font-size: 2rem;
  color: #010606;
  margin-bottom: 20px;

  @media screen and (max-width: 480px) {
    font-size: 1.5rem;
  }
`;

export const ProductsH2 = styled.h2`
  color: #fff;
  font-size: 1rem;
  margin-bottom: 10px;
`;

export const ProductsP = styled.p`
  color: #fff;
  font-size: 1rem;
  text-align: center;
`;

export const ProductsBtn = styled.div`
  display: flex;
  align-items: center;

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const ProductsBtnLink = styled(LinkR)`
  border-radius: 30px;
  background: #deb65a;
  white-space: nowrap;
  padding: 10px 22px;
  color: #00567c;
  font-size: 16px;
  outline: none;
  border: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: #fff;
    color: #00567c;
  }
`;
