import React from "react";
import Icon1 from "../../images/svg-3.svg";
import Icon2 from "../../images/svg-4.svg";
import Icon3 from "../../images/svg-6.svg";
import {
  ProductsContainer,
  ProductsH1,
  ProductsWrapper,
  ProductsCard,
  ProductsIcon,
  ProductsH2,
  ProductsP,
  ProductsBtn,
  ProductsBtnLink,
} from "./ProductsElement";

const Products = () => {
  return (
    <ProductsContainer id="products">
      <ProductsH1>Our Premium Products</ProductsH1>
      <ProductsWrapper>
        <ProductsCard>
          <ProductsIcon src={Icon1} />
          <ProductsH2>Product A</ProductsH2>
          <ProductsP>Product A description here...</ProductsP>
          <ProductsBtn>
            <ProductsBtnLink to="/login">I Want this</ProductsBtnLink>
          </ProductsBtn>
        </ProductsCard>
        <ProductsCard>
          <ProductsIcon src={Icon2} />
          <ProductsH2>Product B</ProductsH2>
          <ProductsP>Product B description here...</ProductsP>
          <ProductsBtn>
            <ProductsBtnLink to="/login">I Want this</ProductsBtnLink>
          </ProductsBtn>
        </ProductsCard>
        <ProductsCard>
          <ProductsIcon src={Icon3} />
          <ProductsH2>Product C</ProductsH2>
          <ProductsP>Product C description here...</ProductsP>
          <ProductsBtn>
            <ProductsBtnLink to="/login">I Want this</ProductsBtnLink>
          </ProductsBtn>
        </ProductsCard>
      </ProductsWrapper>
    </ProductsContainer>
  );
};

export default Products;
