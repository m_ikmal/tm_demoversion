import React from "react";
import {
  Container,
  Form,
  FormButton,
  FormContent,
  FormH1,
  FormInput,
  FormLabel,
  FormWrap,
  Icon,
  Text,
} from "./LoginElements";

const LogIn = () => {
  return (
    <React.Fragment>
      <Container>
        <FormWrap>
          <Icon to="/">TM</Icon>
          <FormContent>
            <Form action="#">
              <FormH1>Log In to your account</FormH1>
              <FormLabel htmlFor="for">Email</FormLabel>
              <FormInput type="email" required />
              <FormLabel htmlFor="for">Password</FormLabel>
              <FormInput type="password" required />
              <FormButton type="submit">Continue</FormButton>
              <Text>Forgot Password?</Text>
            </Form>
          </FormContent>
        </FormWrap>
      </Container>
    </React.Fragment>
  );
};

export default LogIn;
