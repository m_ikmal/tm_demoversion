import React from "react";
import LogIn from "../components/Login";
import ScrollToTop from "../components/ScrollToTop";

const LoginPage = () => {
  return (
    <React.Fragment>
      <ScrollToTop />
      <LogIn />
    </React.Fragment>
  );
};

export default LoginPage;
