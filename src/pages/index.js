import React, { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Footer from "../components/Footer";
import HeroSection from "../components/HeroSection";
import InfoSection from "../components/InfoSection";
import { claims, faq } from "../components/InfoSection/Data";
import Navbar from "../components/Navbar";
import Products from "../components/Products";
import Sidebar from "../components/Sidebar";

const Home = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <Router>
      <Sidebar isOpen={isOpen} toggle={toggle} />
      <Navbar toggle={toggle} />
      <HeroSection />
      <Products />
      <InfoSection {...faq}/>
      <InfoSection {...claims}/>
      <Footer />
    </Router>
  );
};

export default Home;
